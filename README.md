# ESERCITAZIONI PRATICHE DEL CORSO DI BIOINFORMATICA ED ANALISI FUNZIONALE DEL GENOMA   

## Programma
### Esercitazione 1 (10/11/2022 e 17/11/2022)
#### Banche dati: definizioni generali e sistemi di retrieval
- [X] [Introduzione alla Bioinformatica](Intro_Bioinfo.md)
- [X] [Definizione di Database e struttura delle entry **GenBank**](Banche_dati/banche_dati.md)  
- [X] [Sistemi di retrieval](Banche_dati/retrieval.md)  
- [X] [PubMed](Banche_dati/pubmed.md)

### Esercitazione 2 (24/11/2022 e 01/12/2022)
#### Allineamento tra sequenze
- [X] [Allineamento](Allineamento_tra_sequenze/allineamento.md)
- [X] [Utilizzo di BLAST e BLAT per l'analisi genomica](Allineamento_tra_sequenze/blast_genomico.md)

### Esercitazione 3 (06/12/2022 - 09/01/2023)
#### Genome Browser 
- [X] [ENSEMBL](Genome_Browser/Ensembl.md)
- [X] [UCSC](Genome_Browser/UCSC.md)
- [X] [Connessione al server e introduzione al BASH](BASH/Accesso_al_server.md)  

### Esercitazione 4 (11-13/01/2023)
### DNA Metabarcoding
- [X] [DNA Metabarcoding](Metabarcoding/metabarcoding.md)

### Esercitazione 5 (16-18/01/2023)
- [X] [DNA Metabarcoding](Metabarcoding/metabarcoding.md)
- [X] [Genome Assembly](Genome_Assembly/genome_assembly.md)

### Esercitazione 6 (20-24/01/2023)
- [X] [Genome Assembly](Genome_Assembly/genome_assembly.md)

### [Relazioni](relazioni.md)  
