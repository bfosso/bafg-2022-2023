# RELAZIONE

## Italiano :it:
Ogni studente dovrà presentare una relazione relativa ai laboratori svolti entro **5gg LAVORATIVI** dalla data in cui è previsto l’esame orale.  

---
Prendiamo ad esempio l' esame previsto per il venerdì 10/02.   
 
|            06 Febbraio            |07 Febbraio|08 Febbraio|09 Febbraio|10 Febbraio|
|:---------------------------------:|:-----------:|:-----------:|:---:|:---------:|
|              Lunedì               |Martedì|Mercoledì|Giovedì|  Venerdì  |
|            LAVORATIVO             |LAVORATIVO|LAVORATIVO|LAVORATIVO| **ESAME** |
| **LIMITE ULTIMO PER LA CONSEGNA** |        3    |2|1|     1     |1|
---

La relazione dovrà riguardare **tutte** le seguenti tematiche:
1. Descrizione della procedura bioinformatica relativa all’analisi di dati di DNA-Metabarcoding e discussione dei dati ottenuti:
   1. Vanno comparate sia le metodologie bioinformatiche che i risultati ottenuti durante l' esercitazione con quelli presentati nel lavoro [Ravegnini et al. 2020](https://www.mdpi.com/1422-0067/21/24/9735);  
2. Descrizione della procedura bioinformatica per l’assemblaggio e annotazione di un genoma batterico corredata da un breve commento dei risultati ottenuti.
   1. Tra le sequenze proteiche identificate da **PROKKA** sceglierne una e utilizzando uno dei tool che abbiamo visto durante le esercitazioni verificare che la predizione funzionale sia corretta.   
3. Tra i geni elencati nell'[Annex 1](#annex-1) dovete sceglierne uno e indicare le seguenti informazioni:
   - Localizzazione cromosomica comprensiva di coordinate e strand;  
   - Numero di trascritti annotati;  
     - Considerando il trascritto codificante proteina più lungo dovete indicare:  
       * l' identificativo del trascritto;  
       * il numero di Esoni che partecipano alla formazione di quel trascritto;
       * Utilizzando la sequenza proteica identificate la presenza di possibile proteine omologhe.
     - Qualora si tratti di un gene non codificante proteina descrivete il prodotto. Utilizzando il blast dovrete trovare eventuali trascritti omologhi in banca dati e definire se anche questi siano non codificanti proteina.
   - Numero di Ortologhi annotati.  
             

***Per ciascuna dei tre punti non si dovranno superare le 5 pagine e per ciascuna di esse potete inserire al massimo 5 immagini***.  

## English :gb:
Each student must submit a report describing the laboratories carried out within **5 WORKING days** from the date on which the oral exam is scheduled.

---
Let's consider for istance the exam scheduled On Friday  10/02.


|         Febraury 6th         | Febraury 7th | Febraury 8th | Febraury 9th | Febraury 10th |
|:----------------------------:|:------------:|:------------:|:------------:|:-------------:|
|            Monday            |   Tuesday    |  Wednesday   |   Thursday   |    Friday     |
|         Working day          |  Working day  |  Working day  |  Working day  |   **Exam**    |
| **Last to send the reports** |      3       |      2       |      1       |       1       |1|
---

The report must cover **all** the following topics:
1. Description of the bioinformatic procedure related to the analysis of DNA-Metabarcoding data and the discussion of the obtained results:
    1. Both the bioinformatics methodologies and the results obtained during the exercise have to be compared with those presented in the paper [Ravegnini et al. 2020](https://www.mdpi.com/1422-0067/21/24/9735);
2. Description of the bioinformatic procedure for the assembly and annotation of a bacterial genome with a brief comment on the obtained results.
    1. Among the protein sequences identified by **PROKKA** choose one and using one of the tools we learned to use during the practices verify whether the functional prediction is correct.
3. Among the genes listed in [Annex 1](#annex-1) you must choose one and indicate the following information:
    - Chromosome localization including coordinates and strands;
    - Number of annotated transcripts;
      - Considering the longest protein coding transcript you must indicate:
        * the identifier of the transcript;
        * the number of Exons participating in the formation of that transcript;
        * Using the protein sequence identify the presence of possible homologous proteins.
      - If you choose a non-protein coding gene, describe the product. Using the BLAST you have to find any homologous transcripts in the database and define whether these are non-protein coding too.
    - Number of orthologs annotated. 

***For each of the three themes they must not exceed 5 pages and for each of them you can insert a maximum of 5 images***.

## Annex 1
Tabella di Geni target

|target_id|Gene ID|
|:---:|:---|
|ENSG00000169064|ZBBX|
|ENSG00000160401|CFAP157|
|ENSG00000186973|FAM183A|
|ENSG00000118492|ADGB|
|ENSG00000155761|SPAG17|
|ENSG00000142621|FHAD1|
|ENSG00000077327|SPAG6|
|ENSG00000288407|CCDC33|
|ENSG00000140481|CCDC33|
|ENSG00000255794|RMST|
|ENSG00000179813|FAM216B|
|ENSG00000183273|CCDC60|
|ENSG00000181378|CFAP65|
|ENSG00000154099|DNAAF1|
|ENSG00000034239|EFCAB1|
|ENSG00000285501|AGBL2|
|ENSG00000135218|CD36|
|ENSG00000230062|ANKRD66|
|ENSG00000165923|AGBL2|
|ENSG00000227460|SYNGAP1|
|ENSG00000152582|SPEF2|
|ENSG00000245067|IGFBP7-AS1|
|ENSG00000183644|HOATZ|
|ENSG00000176601|MAP3K19|
|ENSG00000197826|CFAP299|
|ENSG00000206199|ANKUB1|
|ENSG00000215187|FAM166B|
|ENSG00000152760|DYNLT5|
|ENSG00000163395|IGFN1|
|ENSG00000169126|ODAD2|
|ENSG00000197057|DTHD1|
|ENSG00000224116|INHBA-AS1|
|ENSG00000243910|TUBA4B|
|ENSG00000088727|KIF9|
|ENSG00000182648|LINC01006|
|ENSG00000258752||
|ENSG00000168658|VWA3B|
|ENSG00000126838|PZP|
|ENSG00000141294|LRRC46|
|ENSG00000170959|DCDC1|
|ENSG00000133665|DYDC2|
|ENSG00000163071|SPATA18|
|ENSG00000186471|AKAP14|
|ENSG00000167646|DNAAF3|
|ENSG00000176029|C11orf16|
|ENSG00000171595|DNAI2|
|ENSG00000132554|RGS22|
|ENSG00000171962|DRC3|
|ENSG00000163075|CFAP221|
|ENSG00000288097||
|ENSG00000131089|ARHGEF9|
|ENSG00000185055|EFCAB10|
|ENSG00000188219|POTEE|
|ENSG00000206299|TAP2|
|ENSG00000137098|SPAG8|
|ENSG00000133115|STOML3|
|ENSG00000102900|NUP93|
|ENSG00000089101|CFAP61|
|ENSG00000139304|PTPRQ|
|ENSG00000196604|POTEF|
|ENSG00000152611|CAPSL|
|ENSG00000105479|ODAD1|
|ENSG00000176697|BDNF|
|ENSG00000140527|WDR93|
|ENSG00000112319|EYA4|
|ENSG00000166596|CFAP52|
|ENSG00000160838|LRRC71|
|ENSG00000205838|TTC23L|
|ENSG00000178053|MLF1|
|ENSG00000164746|C7orf57|
|ENSG00000163521|GLB1L|
|ENSG00000274205|MAPK15|
|ENSG00000204711|C9orf135|
|ENSG00000169282|KCNAB1|
|ENSG00000204022|LIPJ|
|ENSG00000091136|LAMB1|
|ENSG00000187726|DNAJB13|
|ENSG00000162910|MRPL55|
|ENSG00000158486|DNAH3|
|ENSG00000228162||
|ENSG00000105877|DNAH11|
|ENSG00000197748|CFAP43|
|ENSG00000091181|IL5RA|
|ENSG00000140795|MYLK3|
|ENSG00000197168|NEK5|
|ENSG00000162814|SPATA17|
|ENSG00000165309|ARMC3|
|ENSG00000153789|CIBAR2|
|ENSG00000188596|CFAP54|
|ENSG00000162004|CCDC78|
|ENSG00000122735|DNAI1|
|ENSG00000110195|FOLR1|
|ENSG00000180638|SLC47A2|
|ENSG00000143355|LHX9|
|ENSG00000274583|FAM74A4|
|ENSG00000165533|TTC8|
|ENSG00000177989|ODF3B|
|ENSG00000205084|TMEM231|
|ENSG00000118307|DNAI7|
|ENSG00000153347|FAM81B|
|ENSG00000168356|SCN11A|
|ENSG00000124143|ARHGAP40|
|ENSG00000187492|CDHR4|
|ENSG00000244227|LRRC77P|
|ENSG00000178125|PPP1R42|
|ENSG00000110436|SLC1A2|
|ENSG00000196118|CCDC189|
|ENSG00000226321|CROCC2|
|ENSG00000231557||
|ENSG00000128536|CDHR3|
|ENSG00000145491|ROPN1L|
|ENSG00000123977|DAW1|
|ENSG00000164099|PRSS12|
|ENSG00000138400|MDH1B|
|ENSG00000030304|MUSK|
|ENSG00000102317|RBM3|
|ENSG00000133640|LRRIQ1|
|ENSG00000188523|CFAP77|
|ENSG00000272398|CD24|
|ENSG00000150773|PIH1D2|
|ENSG00000213085|CFAP45|
|ENSG00000225746|MEG8|
|ENSG00000164694|FNDC1|
|ENSG00000181085|MAPK15|
|ENSG00000230598||
|ENSG00000168589|DYNLRB2|
|ENSG00000184500|PROS1|
|ENSG00000287427||
|ENSG00000084623|EIF3I|
|ENSG00000163885|CFAP100|
|ENSG00000284862|CCDC39|
|ENSG00000111837|MAK|
|ENSG00000109654|TRIM2|
|ENSG00000130414|NDUFA10|
|ENSG00000188517|COL25A1|
|ENSG00000159212|CLIC6|
|ENSG00000153237|CCDC148|
|ENSG00000167858|TEKT1|
|ENSG00000115194|SLC30A3|
|ENSG00000105792|CFAP69|
|ENSG00000145632|PLK2|
|ENSG00000162643|DNAI3|
|ENSG00000172724|CCL19|
|ENSG00000164972|C9orf24|
|ENSG00000153930|ANKFN1|
|ENSG00000233730|LINC01765|
|ENSG00000152503|TRIM36|
|ENSG00000283022|HYDIN|
|ENSG00000179133|C10orf67|
|ENSG00000227954|TARID|
|ENSG00000096093|EFHC1|
|ENSG00000157423|HYDIN|
|ENSG00000170324|FRMPD2|



[Programma](../README.md) 